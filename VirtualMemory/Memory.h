#include <iostream>

using namespace std;

class Memory() {

private:
    PageTableEntry *frames[];

public:

    Memory();
    void setFrame(int frame, PageTableEntry *pte);
    PageTableEntry *getFrame(int frame);
    int getFreePage();
    int findSwapPage();
    int convert(int frame);

}

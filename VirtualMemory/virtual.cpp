#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <vector>

using namespace std;

int main() {

    ifstream ifs;
    ifs.open("VMInput.txt");

    int processTable[8];

    if(!ifs.good()) {
        return 1;
    }

    while(!ifs.eof()) {

        string s;
        getline(ifs, s);

        vector<string> tokens;
        stringstream stream;
        stream << s;
        for(string str; stream >> s;) {

            tokens.push_back(s);

        }

        if(!tokens[0].compare("new")) {

            cout << "new " << tokens[1] << endl;

        } else if(!tokens[0].compare("switch")) {

            cout << "switch to " << tokens[1] << endl;

        } else {

            cout << "access " << tokens[1] << endl;

        }

    }

    ifs.close();

}

#include <stdio.h>
#include <stdlib.h>

int foo();
int *bar();

int main(int argc, char **argv) {

    printf("Number of args: %i\n", argc);

    char buffer[100] = "this is something";

    for(int i=0; i<argc; i++) {
        printf("Arg %i: %s\n", i, argv[i]);
    }

    int a = 20;
    int *p = &a;

    *p = 23;

    printf("a = %i\n", a);
    printf("p = %p\n", p);

    int result = foo();
    printf("Result: %i\n", result);

    printf("Buffert[0] %c\n", buffer[5]);

    int *myarr = bar();
    printf("myarray[5] = %i\n", myarr[5]);
    free(myarr);
}

int foo() {
    int a, b;

    a = 5;
    b = 6;

    return a+b;
}

int *bar() {

    int *arr = calloc(10,sizeof(int));
    for(int i=0; i<10; i++) {
        arr[i] = i;
    }
    return arr;
}

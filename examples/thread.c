#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

void *thread_function(void *arg);

int sum = 0;
sem_t sum_lock;

int main(int argc, char **argv) {

    int num_threads;
    pthread_t *threads;
    long *tid;

    if(argc < 2) {

        printf("Usage: %s [NUMBER IF THREADS]\n", argv[0]);

        return 0;
    }

    num_threads = atoi(argv[1]);
    printf("Launching %i threads.\n", num_threads);

    threads = (pthread_t*) calloc(num_threads, sizeof(pthread_t));

    if(sem_init(&sum_lock, 0, 1)) {

        printf("Error initializing semaphore\n");
        perror("sem_init");

    }

    int pcr;
    for(long i=0; i < num_threads; i++) {

        if( (pcr = pthread_create(&threads[i], NULL, &thread_function,
                                 (void*)i))) {

            printf("Create Failed: %ld", i);

        }

    }

    printf("Done launching threads\n");

    for (int i=0; i<num_threads; i++) {

        pthread_join(threads[i], NULL);

    }

    printf("Sum: %i\n", sum);
    free(threads);
}

void *thread_function(void * arg) {

    long my_id = (long) arg;
    //printf("ID: %ld\n", my_id);

    for(int i=0; i < 10; i++) {
        sem_wait(&sum_lock);
        int tmp = sum;
        tmp++;
        usleep(1);
        sum = tmp;
        sem_post(&sum_lock);

    }
    printf("ID: %ld done\n", my_id);

}

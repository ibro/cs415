#include <iostream>
#include <string>
#include "Animal.h"

using namespace std;

void setupArr(int *fillArr, int count);

class Animal {

public:
    string name;
    string color;

    void printInfo();
};

void Animal::printInfo() {

    cout << "Name: " << name << endl;
    cout << "Color: " << color << endl;

}

int main(int argc, char **argv){

   cout << "Hello world!" << endl;

    int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int array[10];
    setupArr(array, 10);

    cout<< "Array[5]" << arr[5] << endl;
    cout << "Array[4]" << array[4] << endl;

    Animal fred("Fred", "White");
    fred.printInfo();
}

void setupArr(int *fillArr, int count) {

    for(int i = 0; i < count; i++) {

        fillArr[i] = i+1;

    }

}

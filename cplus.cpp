#include <iostream>
#include <string>
#include "Animal.h"

using namespace std;

void setupArr(int *fillArr, int count);


int main(int argc, char **argv){

   cout << "Hello world!" << endl;

    int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int array[10];
    setupArr(array, 10);

    cout<< "Array[5]" << arr[5] << endl;
    cout << "Array[4]" << array[4] << endl;

    Animal fred; //animal is on stack
    fred.name = "Fred";
    fred.color = "White";
    fred.printInfo();

    Animal *mary = new Animal(); // animal is on heap
    mary->name = "Mary";
    mary->color = "Black";
    (*mary).color = "Red"; //same thing as above
    mary->printInfo();

    delete mary;

}

void setupArr(int *fillArr, int count) {

    for(int i = 0; i < count; i++) {

        fillArr[i] = i+1;

    }

}

#include <iostream>
#include <thread>
#include <mutex>
#include <stdio.h>
#include <stdlib.h>
#include <mutex>
#include <unistd.h>

using namespace std;

mutex mtx;
int A;
int B;

int buffer[3];

void producer(int id) {
    int sleep;
    cout << "I am a producer id=" << id << endl;
    A+=1;
    cout << "A=" << A << " by producer" << endl;
    sleep = rand() % 10 + 1;
    usleep(sleep);
    sleep = 0;
    B+=2;
    cout << "B=" << B << " by producer" << endl;
    sleep = rand() % 10 + 1;
    usleep(sleep);


}

void consumer(int id) {

    int sleep;
    cout << "I am a consumer id=" << id << endl;
    B+=2;
    cout << "B=" << B << endl;
    sleep = rand() % 10 + 1;
    usleep(sleep);
    sleep = 0;
    A+=1;
    cout << "A=" << A <<  endl;
    sleep = rand() % 10 + 1;
    usleep(sleep);

}

void launcher(int arg) {

    int id = arg;
    if (arg%2 == 0 ) {
        producer(id);
    } else {
        consumer(id);
    }

}

int main(int argc, char *argv[]) {

    int num_threads;

    long *tid;

    int n;

    A = 0;
    B = 0;


    if(argc < 2) {

        cout << "Usage: " << argv[0] << " number of threads" << endl;
        return 0;

    }

    num_threads = atoi(argv[1]);

    cout << "Launching " << num_threads << " threads." << endl;

    for(int i = 0; i < num_threads; i++) {

        thread pr (launcher, i);
        pr.join();
    }

    return 0;
}
